/******************************************************************************
 * Euler Problem 640
 * Bob plays a single-player game of chance using two standard 6-sided dice and
 * twelve cards numbered 1 to 12. When the game starts, all cards are placed
 * face up on a table. Each turn, Bob rolls both dice, getting numbers x and y
 * respectively, each in the range 1,...,6. He must choose amongst three
 * options: turn over card x, card y, or card x+y. (If the chosen card is
 * already face down, it is turned to face up, and vice versa.) If Bob manages
 * to have all twelve cards face down at the same time, he wins.
 *
 * Alice plays a similar game, except that instead of dice she uses two fair
 * coins, counting heads as 2 and tails as 1, and that she uses four cards
 * instead of twelve. Alice finds that, with the optimal strategy for her game,
 * the expected number of turns taken until she wins is approximately 5.673651.
 * Assuming that Bob plays with an optimal strategy, what is the expected number
 * of turns taken until he wins? Give your answer rounded to 6 places after the
 * decimal point.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>
//#include <pthread.h>
#include <omp.h>

void InitialiseDecisionArray(void);
bool CanFlip(uint16_t cards, uint8_t toFlip);
uint16_t GetFlag(uint8_t value);
int RunGame();
uint8_t Roll(void);

#define MAX_12_BIT 4095
#define MAX_3_CHOICE 21

#define NUM_OF_ATTEMPTS 200000000
#define NUM_DP_CONSISTENCY 1

uint8_t priority[13] = {0, 7, 8, 9, 10, 11, 12, 6, 5, 4, 3, 2, 1}; // Lower number = higher priority
uint8_t rollIndex[7][7];
uint16_t decisions[MAX_12_BIT][MAX_3_CHOICE];

int main(void) {
    time_t startTime = clock();
    InitialiseDecisionArray();
    printf("Processing time: %d.\n", ((clock() - startTime) * 1000/CLOCKS_PER_SEC));
    
    unsigned long totalGameTurns = 0;
    bool consistent = false;
    startTime = clock();
    
    // #pragma omp parallel shared(totalGameTurns)
    // {        
    //     int gameTurns = 0;
    //     long attempt = 0;
    //     long NUM_OF_ATTEMPTS_PER_THREAD = ((long)NUM_OF_ATTEMPTS / (long)omp_get_num_threads);
    //     while(attempt < NUM_OF_ATTEMPTS_PER_THREAD) {
    //         gameTurns = RunGame();
    //         #pragma omp critical
    //         {
    //             totalGameTurns += RunGame();                
    //         }
    //         attempt++;
    //     };
    // }
    

    omp_init_lock;
    #pragma omp parallel num_threads(2) shared(totalGameTurns)
    {
        printf("Started thread.\n");
        long gameTurns = 0;
        #pragma omp for
        for(long attempt = 0; attempt < NUM_OF_ATTEMPTS; attempt++) {
            /* code */
            gameTurns += RunGame();            
        }

        printf("This thread finished.\n");

        // #pragma omp critical
        // {
        //     totalGameTurns += gameTurns;
        // }
        while(!omp_test_lock) {};
        omp_set_lock;
        totalGameTurns += gameTurns;
        omp_unset_lock;
    }
    
    printf("Completed %i turns in: %d.\n", NUM_OF_ATTEMPTS, ((clock() - startTime) * 1000/CLOCKS_PER_SEC));
    printf("Number of Turns: %u.\n", totalGameTurns);
    printf("Average Turns Required: %f.\n", (float)(totalGameTurns/(float)NUM_OF_ATTEMPTS));

    printf("Press ENTER key to Continue.\n");  
    getchar(); 
	return EXIT_SUCCESS;
}

void InitialiseDecisionArray(void) {
    int index = 0;
    uint8_t total = 0;
    int p1, p2, pt = 0;
    bool flipped = false;
    for(uint8_t ii = 1; ii <= 6; ii++) {        
        printf("First roll: %i.\n", ii);
        printf("Starting second roll combination for %i.\n", ii);
        for(uint8_t ij = ii; ij <= 6; ij++) {
            printf("Second roll %i.\n", ij);
            rollIndex[ii][ij] = index;
            rollIndex[ij][ii] = index;
            total = ii + ij;
            p1 = priority[ii];
            p2 = priority[ij];
            pt = priority[total];
            printf("Creating decisions for %i, %i, %i combination.\n", ii, ij, total);
            for(uint16_t ik = 0; ik < MAX_12_BIT; ik++) {                 
                flipped = false; 

                // find any unflipped
                if(p1 <= p2 && p1 < pt) { // p1 is lowest 
                    if(CanFlip(ik, ii)) { // if we haven't flipped ii, flip
                        decisions[ik][index] = ik ^ GetFlag(ii);
                        flipped = true;
                    } else if(p2 < pt) { // if p2 is next lowest
                        if(CanFlip(ik, ij)) { // if we haven't flipped ij, flip
                            decisions[ik][index] = ik ^ GetFlag(ij);
                            flipped = true;
                        } else if (CanFlip(ik, total)) { // pt  is last, if we haven't flipped, flip
                            decisions[ik][index] = ik ^ GetFlag(total);
                            flipped = true;
                        }
                    } else { // pt is next lowest after p1
                        if(CanFlip(ik, total)) { // if we haven't flipped total, flip
                            decisions[ik][index] = ik ^ GetFlag(total);
                            flipped = true;
                        } else if (CanFlip(ik, ij)) { // p2 is last, if we haven't flipped flip
                            decisions[ik][index] = ik ^ GetFlag(ij);  
                            flipped = true;                          
                        }
                    } 
                } else if(p2 < p1 && p2 < pt) {
                    if(CanFlip(ik, ij)) { // if we haven't flipped ij, flip
                        decisions[ik][index] = ik ^ GetFlag(ij);
                        flipped = true;
                    } else if(p1 < pt) { // if p1 is next lowest
                        if(CanFlip(ik, ii)) { // if we haven't flipped ii, flip
                            decisions[ik][index] = ik ^ GetFlag(ii);  
                            flipped = true;          
                        } else if (CanFlip(ik, total)) { // pt  is last, if we haven't flipped, flip
                            decisions[ik][index] = ik ^ GetFlag(total);  
                            flipped = true;          
                        }
                    } else { // pt is next lowest after p2
                        if(CanFlip(ik, total)) { // if we haven't flipped total, flip
                            decisions[ik][index] = ik ^ GetFlag(total);  
                            flipped = true;          
                        } else if (CanFlip(ik, ii)) { // p1 is last, if we haven't flipped flip
                            decisions[ik][index] = ik ^ GetFlag(ii);   
                            flipped = true;                                     
                        }
                    } 
                } else if(pt < p1 && pt < p2) {
                    if(CanFlip(ik, total)) { // if we haven't flipped total, flip
                        decisions[ik][index] = ik ^ GetFlag(total);
                        flipped = true;
                    } else if(p1 <= p2) { // if p1 is next lowest
                        if(CanFlip(ik, ii)) { // if we haven't flipped ii, flip
                            decisions[ik][index] = ik ^ GetFlag(ii);  
                            flipped = true;          
                        } else if (CanFlip(ik, ij)) { // p2  is last, if we haven't flipped, flip
                            decisions[ik][index] = ik ^ GetFlag(ij);  
                            flipped = true;          
                        }
                    } else { // p2 is next lowest after pt
                        if(CanFlip(ik, ij)) { // if we haven't flipped total, flip
                            decisions[ik][index] = ik ^ GetFlag(ij);  
                            flipped = true;          
                        } else if (CanFlip(ik, ii)) { // p1 is last, if we haven't flipped flip
                            decisions[ik][index] = ik ^ GetFlag(ii);   
                            flipped = true;                                     
                        }
                    } 
                }

                if(!flipped) {
                    if(p1 >= p2 && p1 > pt) {
                        decisions[ik][index] = ik - (GetFlag(ii));
                    } else if(p2 > p1 && p2 > pt) {
                        decisions[ik][index] = ik - (GetFlag(ij));
                    } else if(pt > p1 && pt > p2) {
                        decisions[ik][index] = ik - (GetFlag(total));
                    }
                }        
            }
            index++;
            printf("Finished decisions for %i, %i, %i combination.\n", ii, ij, total);
        }
        printf("Finished second roll combinations.\n");
    }
    printf("Final index value: %i.\n", index);
    printf("All decisions calculated.\n");
}

bool CanFlip(uint16_t cards, uint8_t toFlip) {
    uint16_t flag = GetFlag(toFlip);
    if((cards & flag) == 0)
        return true;
    
    return false;
}

uint16_t GetFlag(uint8_t value) {
    return (0x01 << (value - 1));
}

int RunGame() {
    int numTurns = 0;
    uint8_t dice1 = 0;
    uint8_t dice2 = 0;
    uint8_t total = 0;
    uint16_t currentCardState = 0x0000;
    while(currentCardState != MAX_12_BIT) {
        dice1 = Roll();
        dice2 = Roll();
        currentCardState = decisions[currentCardState][(rollIndex[dice1][dice2])];
        numTurns++;
    }
    return numTurns;
}

uint8_t Roll(void) {
    uint8_t roll = rand() % 6;
    if(roll > 0)
        return roll;
    return 6;
}
